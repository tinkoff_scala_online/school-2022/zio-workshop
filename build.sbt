ThisBuild / scalaVersion := "2.13.8"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.tfs"
ThisBuild / organizationName := "example"

val ZHTTPVersion = "2.0.0-RC7"
val circe = "0.14.1"
val slf4j = "1.7.30"
val logback = "1.2.10"
val zioLogging = "2.0.0-RC8"

lazy val root = (project in file("."))
  .settings(
    name := "ZioWorkshop",
    libraryDependencies ++= Seq(
      "dev.zio" %% "zio" % "2.0.0-RC5",
      "dev.zio" %% "zio-test" % "2.0.0-RC5" % Test,
      "io.d11" %% "zhttp" % ZHTTPVersion,
      "io.d11" %% "zhttp-test" % ZHTTPVersion % Test,
      "io.circe" %% "circe-core" % circe,
      "io.circe" %% "circe-generic" % circe,
      "io.circe" %% "circe-parser" % circe,
      "ch.qos.logback" % "logback-classic" % logback,
      "ch.qos.logback" % "logback-core" % logback,
      "org.slf4j" % "slf4j-api" % slf4j,
      "dev.zio" %% "zio-logging-slf4j" % zioLogging,
      "dev.zio" %% "zio-logging" % zioLogging,
      "io.getquill" %% "quill-jdbc-zio" % "3.17.0-RC3",
      "io.github.kitlangton" %% "zio-magic" % "0.3.11",
      "org.postgresql" % "postgresql" % "42.3.1"
    ),
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")
  )
