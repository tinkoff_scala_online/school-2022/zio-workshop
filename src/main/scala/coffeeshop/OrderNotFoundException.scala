package coffeeshop

import java.util.UUID

case class OrderNotFoundException(coffee: UUID) extends Throwable
