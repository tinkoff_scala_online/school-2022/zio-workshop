package coffeeshop

import io.getquill._
import zio.{Task, ULayer, ZIO, ZLayer}

import java.util.UUID
import javax.sql.DataSource
import scala.collection.concurrent.TrieMap

object InMemoryOrderStore {
  val live: ULayer[OrderStore] =
    ZLayer.succeed[OrderStore](InMemoryOrderStore())
}

case class InMemoryOrderStore() extends OrderStore {

  private val map = TrieMap[UUID, CoffeeOrder]()

  override def save(id: UUID, coffeeOrder: CoffeeOrder): Task[Unit] =
    ZIO
      .attempt(map.put(id, coffeeOrder))
      .unit
      .orElseFail(new Exception("save error"))

  override def get(id: UUID): Task[Option[CoffeeOrder]] = Task
    .succeed(map.get(id))
}

case class PostgresOrderStore(dataSource: DataSource) extends OrderStore {

  private val ctx = new PostgresZioJdbcContext(SnakeCase)
  import ctx._

  private val orders = quote {
    querySchema[CoffeeOrder]("orders")
  }

  override def save(id: UUID, coffeeOrder: CoffeeOrder): Task[Unit] = {
    ctx
      .run(orders.insertValue(lift(coffeeOrder)))
      .provideService(dataSource)
      .unit
  }

  override def get(id: UUID): Task[Option[CoffeeOrder]] = {
    val coffeeType = "black"
    ctx
      .run(orders.filter(order => order.name == lift(coffeeType)).take(1))
      .map(_.headOption)
      .provideService(dataSource)
  }
}

object PostgresOrderStore {
  val live: ZLayer[DataSource, Nothing, OrderStore] =
    ZLayer.fromFunction(PostgresOrderStore(_))
}
