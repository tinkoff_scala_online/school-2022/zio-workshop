package coffeeshop

import zio.{Accessible, RIO, Task, ZIO}

import java.util.UUID

trait CoffeeShopService {
  def makeCoffee(coffeeOrder: CoffeeOrder): Task[UUID]

  def getCoffeeOrder(id: UUID): Task[Option[CoffeeOrder]]
}

object CoffeeShopService extends Accessible[CoffeeShopService] {}
