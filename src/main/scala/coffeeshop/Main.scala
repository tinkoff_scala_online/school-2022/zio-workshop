package coffeeshop

import com.zaxxer.hikari.HikariDataSource
import zhttp.service.Server
import zio.logging.LogFormat
import zio.logging.backend.SLF4J
import zio.{
  LogLevel,
  RuntimeConfigAspect,
  Scope,
  ZIO,
  ZIOAppArgs,
  ZIOAppDefault,
  ZLayer
}

object Main extends ZIOAppDefault {

  val aspect: RuntimeConfigAspect =
    SLF4J.slf4j(
      LogLevel.Debug,
      format = LogFormat.colored
    )

  override def hook = aspect

  override def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] = {

    val routeContainer = new Routes
    val routes = routeContainer.allRoutes
    val source = new HikariDataSource()
    source.setJdbcUrl("jdbc:postgresql://localhost/tfs")
    source.setUsername("tfs")
    source.setPassword("tfs")
    val dataSource = ZLayer.succeed(source)
    val db = dataSource >>> PostgresOrderStore.live
    val env =
      (db ++ NotificationServiceImpl.live) >>> CoffeeShopServiceImpl.live

    Server.start(
      8080,
      routes.provideLayer(env)
    )

  }

}
