package coffeeshop

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder
import io.circe.parser.decode
import io.circe.syntax.EncoderOps
import zhttp.http.HttpError.InternalServerError
import zhttp.http._
import zio.ZIO

import java.util.UUID

case class CreateCoffeeOrderResult(id: UUID)

object CreateCoffeeOrderResult {
  implicit val encoder: Encoder[CreateCoffeeOrderResult] = deriveEncoder
}

class Routes {
  private val helloRoutes = Http.collectZIO[Request] {
    case Method.GET -> !! / "hello" =>
      ZIO
        .attempt("hello2")
        .fold(
          _ => Response.fromHttpError(InternalServerError("error")),
          str => Response.text(str)
        )
  }

  private val coffeeRoutes = Http.collectZIO[Request] {
    case req @ Method.POST -> !! / "coffee" =>
      (for {
        jsonStr <- req.bodyAsString
        order <- ZIO.fromEither(decode[CoffeeOrder](jsonStr))
        id <- CoffeeShopService(_.makeCoffee(order))
        result = CreateCoffeeOrderResult(id)
      } yield result)
        .tapError(err => ZIO.logError(err.toString))
        .fold(
          _ =>
            Response.fromHttpError(
              InternalServerError("error")
            ),
          result =>
            Response.json(result.asJson.toString()).setStatus(Status.Created)
        )

    case req @ Method.GET -> !! / "coffee" / id => {
      CoffeeShopService(_.getCoffeeOrder(UUID.fromString(id))).fold(
        _ =>
          Response.fromHttpError(
            InternalServerError("error")
          ),
        {
          case Some(response) => Response.json(response.asJson.toString())
          case None           => Response.fromHttpError(HttpError.NotFound(req.path))
        }
      )
    }
  }

  val allRoutes = helloRoutes ++ coffeeRoutes
}
