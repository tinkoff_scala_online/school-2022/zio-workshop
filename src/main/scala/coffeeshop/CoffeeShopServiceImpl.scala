package coffeeshop

import zio.Console.printLine
import zio.ZIO.{log, logInfo, serviceWithZIO}
import zio.{Random, Task, ULayer, ZIO, ZLayer}

import java.util.UUID
import scala.collection.concurrent.TrieMap

trait OrderStore {
  def save(id: UUID, coffeeOrder: CoffeeOrder): Task[Unit]

  def get(id: UUID): Task[Option[CoffeeOrder]]
}

trait NotificationService {
  def send(msg: String): Task[Unit]
}

case class NotificationServiceImpl() extends NotificationService {
  override def send(msg: String): Task[Unit] = printLine("notity...")
}

object NotificationServiceImpl {
  val live = ZLayer.succeed[NotificationService](new NotificationServiceImpl())
}

case class CoffeeShopServiceImpl(
    orderStore: OrderStore,
    notificationService: NotificationService
) extends CoffeeShopService {
  def makeCoffee(coffeeOrder: CoffeeOrder): Task[UUID] = {
    for {
      _ <- logInfo("making coffee")
      random <- ZIO.random
      id <- random.nextUUID
      _ <- orderStore.save(id, coffeeOrder)
      _ <- notificationService.send("done!")
    } yield id
  }

  def getCoffeeOrder(id: UUID): Task[Option[CoffeeOrder]] = {
    for {
      es <- orderStore.get(id)
    } yield es

  }

}

object CoffeeShopServiceImpl {
  val live =
    ZLayer.fromFunction(CoffeeShopServiceImpl(_, _))
}
